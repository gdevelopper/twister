SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `Twistiti` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `Twistiti`;

CREATE TABLE `friend` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_friend` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `friend` (`id`, `id_user`, `id_friend`) VALUES
(1, 1, 2),
(5, 1, 3);

CREATE TABLE `online` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `user_key` varchar(255) DEFAULT NULL,
  `is_root` int(11) DEFAULT NULL,
  `date_connect` TIME 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `online` (`id`, `id_user`, `user_key`, `is_root`) VALUES
(1, 1, '94a841d9-e1c4-4c28-94f9-47f3ef1f7832', 0),
(7, 2, '26e473cb-e5d6-4cc4-b7d3-9f227267a8bb', 0);

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `psw` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id_user`, `login`, `nom`, `prenom`, `psw`) VALUES
(1, 'Toto22', 'Toto', 'Totoz', 0x313233),
(2, 'Toto21', 'Toto', 'Totoz21', 0x313233),
(3, 'Toto23', 'Toto', 'Totoz23', 0x313233),
(4, 'Toto24', 'toto', 'toto', 0x313233);


ALTER TABLE `friend`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_user_2` (`id_user`,`id_friend`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_friend` (`id_friend`);

ALTER TABLE `online`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);


ALTER TABLE `friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `friend`
  ADD CONSTRAINT `friend_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `friend_ibfk_2` FOREIGN KEY (`id_friend`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `online`
  ADD CONSTRAINT `online_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
