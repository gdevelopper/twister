package services;


import org.json.simple.JSONObject;

import bd.FriendTools;
import bd.UserTools;
import servicesTools.ErrorTools;

public class Friend {

	@SuppressWarnings("unchecked")
	public static JSONObject addFriend(String key,String loginFriend) {
		JSONObject ret = new JSONObject();
		int id_user = UserTools.getIdUserK(key);
		if(UserTools.userExists(id_user) && UserTools.userExists(loginFriend)){
			if(!FriendTools.isFriend(id_user, loginFriend)){
				FriendTools.addFriend(id_user,loginFriend);
				ret.put("status", "OK : Ajout d'ami");
			}else{
				return ErrorTools.serviceRefused("Ils Sont amis", 1000);
			}
		}else{
			return ErrorTools.serviceRefused("one or both of users doesn't exists", 1000);
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject removeFriend(String key,String loginFriend) {
		JSONObject ret = new JSONObject();
		int id_user = UserTools.getIdUserK(key);
		if(UserTools.userExists(id_user) && UserTools.userExists(loginFriend)){
			if(FriendTools.isFriend(id_user, loginFriend)){
				FriendTools.removeFriend(id_user,loginFriend);
				ret.put("status", "OK : Suppression d'ami");
			}else{
				return ErrorTools.serviceRefused("Ils ne Sont pas amis", 1000);
			}
		}else{
			return ErrorTools.serviceRefused("one or both of users doesn't exists", 1000);
		}
		return ret;
	}
	
	public static JSONObject listFriend(String key) {
		int id_user = UserTools.getIdUserK(key);
		System.out.println(id_user);
		if(!UserTools.userExists(id_user)){
			return ErrorTools.serviceRefused("user doesn't exists", 1000);
		}else{
			return FriendTools.listFriend(key);
		}
	}
	
	
	
	/**
	 * Test
	 */
	
	public static void main(String args[]){
		System.out.println(User.login("Toto29", "123"));
//		System.out.println(addFriend(User.getKey(),"Toto25")); // ça marche !
//		System.out.println(addFriend("0dd18dee-ca63-4207-9561-bcbc0d400a7a","Toto23")); // ça marche !
//		System.out.println(removeFriend(User.getKey(), "Toto25"));  // ça marche
//		System.out.println(listFriend("0dd18dee-ca63-4207-9561-bcbc0d400a7a")); // ça marche
	}
}
