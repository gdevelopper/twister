package services;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import servicesTools.ErrorTools;
import bd.UserTools;

public class User {
	
	private static  String keys;
	
	@SuppressWarnings("unchecked")
	public static JSONObject createUser(String l,String n,String p,String psw){
		JSONObject ret = new JSONObject();
		if(UserTools.userExists(l)){
			return ErrorTools.serviceRefused("user exists", 1000);
		}else{
			ret.put("status", "OK : Inscription");
			UserTools.addToBDUser(l,n,p,psw);
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject login(String login,String psw){
		JSONObject ret = new JSONObject();
		if(!UserTools.userExists(login)){
			return ErrorTools.serviceRefused("user doesn't exists", 1000);
		}
		if(!UserTools.checkPassword(login,psw)){
			return ErrorTools.serviceRefused("wrong login or password", 1000);
		}
		
		int root = UserTools.isRoot(login);
		String key = UserTools.insererConnexion(login,root);
		ret.put("status", "OK : Connexion");
		ret.put("key", key);
		User.keys = key;
		return ret;
	}
	
	public static void logout(String key) {
		UserTools.logout(key);
	}
	
	public static JSONArray getProfil(String login){
		return UserTools.getProfil(login);
	}
	
	public static String getKey() {
		return keys;
	}

	/**
	 * Test
	 */
	
	public static void main(String args[]){
//		System.out.println(createUser("Toto25", "Toto", "Totoz25", "123")); // ça marche !
		System.out.println(createUser("Toto25", "Toto", "Totoz23", "123"));
//		System.out.println(login("Toto21", "123"));  // ça marche
//		logout("User.getKey()"); // ça marche
//		System.out.println(User.getKey());
	}
}
