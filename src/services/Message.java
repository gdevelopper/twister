package services;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import bd.MessageTools;
import bd.UserTools;
import servicesTools.ErrorTools;
import servicesTools.Utils;

@SuppressWarnings("unused")
public class Message {
	
	@SuppressWarnings("unchecked")
	public static JSONObject addMessage(String key, String message){
		JSONObject ret = new JSONObject();
		int id_user = UserTools.getIdUserK(key);
		if(!UserTools.isOnline(id_user) ){
			return ErrorTools.serviceRefused("user doesn't exist or session dead", 1000);
		}else{
			if(MessageTools.addMessage(id_user,message)) {
				ret.put("status", "OK : Ajout de message");
			}else {
				ret.put("status", "KO : Ajout de message");
			}
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject deleteMessage(String key, String idMessage) {
		JSONObject ret = new JSONObject();
		int id_user = UserTools.getIdUserK(key);
		if(!UserTools.isOnline(id_user) || !MessageTools.messageExists(id_user,idMessage)){
			return ErrorTools.serviceRefused("user or message doesn't exists or session dead", 1000);
		}else{
			if(MessageTools.deleteMessage(id_user,idMessage)) {
				ret.put("status", "OK : Suppression de message");
			}else {
				ret.put("status", "KO : Suppression de message");
			}
		}
		return ret;
	}
	
	public static JSONArray listMessage(String key){
			return MessageTools.listMessage(key);
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject addComment(String login, String idMessage, String comment) {
		JSONObject ret = new JSONObject();
		int id_user = UserTools.getIdUserL(login);
		if(!UserTools.isOnline(id_user) || !MessageTools.messageExists(id_user, idMessage)){
			return ErrorTools.serviceRefused("one or both of user and message doesn't exists or session dead", 1000);
		}else{
			if(MessageTools.addComment(id_user,idMessage,comment)) {
				ret.put("status", "OK : Ajout de commentaire");
			}else {
				ret.put("status", "KO : Ajout de commentaire");
			}
		}
		return ret;		
	}
	
	public static void wait(int time){
		Object m = new Object();
		synchronized (m) {
		    try{
		    	m.wait(time);
		    }catch(Exception e){
		    
		    }
		}
	}
	public static void main(String [] args){
		
		System.out.println(User.login("Toto29", "123")); // ça marche
		String key = User.getKey();
		wait(6000); //ça marche
		System.out.println(addMessage(key, "Test : " + Utils.getTime())); // ça marche
//		System.out.println(deleteMessage(key,"5a953af7e4b09f6a7ad5916b")); // ça marche
//		System.out.println(MessageTools.listMessage(key)); // ça marche
//		System.out.println(listMessage(null)); // null = affichage de tous les message ** ça marche
//		System.out.println(addComment("Toto21","5a953eb5e4b0380ccba39e35","ça marche pas")); // ça marche
	}
}
