package bd;

import java.util.Date;
import java.util.GregorianCalendar;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import servicesTools.Utils;

@SuppressWarnings("unused")
public class MessageTools {

	public static boolean addMessage(int id_user, String msg){
		try{
			if(UserTools.isOnline(id_user)){
				DBCollection message = Database.getCollection("message");
				BasicDBObject bdo = new BasicDBObject();
				bdo.put("id_user",id_user);
				bdo.put("date",Utils.getTime());
				bdo.put("content", msg);
				// Ajout de commentaires
				bdo.put("comments", new BasicDBList());
				message.insert(bdo);
				return true;
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Ajout de commentaires : Par ajout d'un collection "comments" à la collection initiale "message"
	 */
	public static boolean addComment(int id_user, String id_message, String comt){
		try{
			if(UserTools.isOnline(id_user)){
				if(ObjectId.isValid(id_message)){
					ObjectId o = new ObjectId(id_message);
					DBCollection message = Database.getCollection("message");
					BasicDBObject bdo = new BasicDBObject("_id",o);
					BasicDBObject comment = new BasicDBObject();
					comment.put("id_user",id_user);
					comment.put("date",Utils.getTime());
					comment.put("comment", comt);
					BasicDBObject push = new BasicDBObject();
					push.put("$push", new BasicDBObject("comments",comment));
					message.update(bdo, push);
					return true;
				}
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	public static boolean deleteMessage(int id_user, String id_message) {
		
		if(messageExists(id_user, id_message)){
			ObjectId o = new ObjectId(id_message);
			DBCollection message = Database.getCollection("message");
			BasicDBObject bdo = new BasicDBObject("id_user",id_user);
			bdo.put("_id", o);			
			message.remove(bdo);
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONArray listMessage(String key){
		BasicDBObject j = new BasicDBObject();
		JSONArray tab_msg = new JSONArray();
		int id_user = UserTools.getIdUserK(key);
		try{
			DBCollection message = Database.getCollection("message");
			BasicDBObject query;
			query =  (id_user == -1) ? new BasicDBObject() : new BasicDBObject("id_user",id_user);
			DBCursor c = message.find(query).limit(20); // les 20 plus récents
			while(c.hasNext()){
				DBObject o = c.next();
				j.put("_id",o.get("_id").toString());
				j.put("id_user",o.get("id_user"));
				j.put("content",o.get("content"));
				j.put("date",o.get("date"));
				j.put("comments", o.get("comments"));
				tab_msg.add(new BasicDBObject(j));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		 return tab_msg;
	}
		
	public static boolean messageExists(int id_user,String id_message) {
		if(UserTools.isOnline(id_user)){
			if(ObjectId.isValid(id_message)) {
				ObjectId o = new ObjectId(id_message);
				DBCollection message = Database.getCollection("message");
				BasicDBObject bdo = new BasicDBObject();
				bdo.put("id_user",id_user);
				bdo.put("_id", o);	
				DBCursor c = message.find(bdo);
				return c.hasNext();
			}
		}
		return false;
	}
}
