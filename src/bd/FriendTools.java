package bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;

public class FriendTools {
	
	public static void addFriend(int idCurr, String loginFriend) {
		try {
			Connection con = Database.getMySQLConnection();
			String query = "insert into friend (id_user,id_friend) values (?, ?)";
			PreparedStatement st = con.prepareStatement(query);
			st.setInt(1, idCurr);
			st.setInt(2,UserTools.getIdUserL(loginFriend));
			st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void removeFriend(int idCurr, String loginFriend) {
		try {
			Connection con = Database.getMySQLConnection();
			String query = "delete from friend where id_user = ? and id_friend = ?";
			PreparedStatement st = con.prepareStatement(query);
			st.setInt(1, idCurr);
			st.setInt(2,UserTools.getIdUserL(loginFriend));
			st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isFriend(int idCurr, String loginFriend){
			try {
				Connection con = Database.getMySQLConnection();
				String query = "select * from friend where id_user = ? and id_friend = ?";
				PreparedStatement st = con.prepareStatement(query);
				st.setInt(1, idCurr);
				st.setInt(2,UserTools.getIdUserL(loginFriend));
				ResultSet rs = st.executeQuery();
				if(rs.next()){
					return true;
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return false;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject listFriend(String key) {
		JSONObject j = new JSONObject();
		try {
			Connection con = Database.getMySQLConnection();
			String query = "select id_user,nom,prenom from user where id_user in (select id_friend from friend where id_user= ?)";
			PreparedStatement st = con.prepareStatement(query);
			st.setInt(1, UserTools.getIdUserK(key));
			ResultSet res = st.executeQuery();
			while(res.next()) {
				j.put(res.getInt(1),res.getString(2)+" "+res.getString(3));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return j;
	}
}
