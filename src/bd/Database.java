package bd;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

public class Database {

	private DataSource dataSource;
	static Database database;
	public Database(String jndiname) throws SQLException {
		try
		{
			dataSource = (DataSource) new InitialContext().lookup("java:comp/env/" + jndiname);
			
		}
		catch (NamingException e) {
			// Handle error that it’s not configured in JNDI.
			throw new SQLException(jndiname + " is missing in JNDI! : " +e.getMessage());
		}
	}
	
	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
	
	
	public static Connection getMySQLConnection() throws SQLException {
		
		if (DBStatic.mysql_pooling==false) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {}
			return (DriverManager.getConnection("jdbc:mysql://"+ DBStatic.mysql_host +":3306/"+DBStatic.mysql_db, DBStatic.mysql_user, DBStatic.mysql_password));
		}
		else
		{ 
			if (database == null) {
				database = new Database("jdbc/db");
			}
			return (database.getConnection());	
		}
	}
	
	public static DBCollection getCollection(String collection) {
		
		Mongo m;
		DBCollection msg = null;
		try {
			m = new Mongo(DBStatic.mongo_url);
			DB db = m.getDB(DBStatic.mongo_db);
			msg = db.getCollection(collection);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return msg;
	}
}

