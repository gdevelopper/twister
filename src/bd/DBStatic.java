package bd;

public class DBStatic {
	/**
	 * MySQL
	 */
	public static boolean mysql_pooling = false;
	public static String mysql_host = "localhost";
	public static String mysql_db = "Twistiti";
	public static String mysql_user = "root";
	public static String mysql_password = "root";
	
	/**
	 * MongoDB
	 */
	
	public static String mongo_url = "localhost";
	public static String mongo_db = "nom_prenom";
	
}
