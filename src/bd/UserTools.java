package bd;

import java.sql.ResultSet;

import org.json.simple.JSONArray;

import services.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;


public class UserTools {
	
	public static boolean userExists(String l){
		try {
			Connection con = Database.getMySQLConnection();
			String query = "Select * from user where login=?";
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, l);
			ResultSet rs = st.executeQuery();
			if(rs.next()){
				return true;
			}
			rs.close();
			con.close();
			st.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean userExists(int id){
		try {
			Connection con = Database.getMySQLConnection();
			String query = "Select * from user where id_user=?";
			PreparedStatement st = con.prepareStatement(query);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if(rs.next()){
				return true;
			}
			con.close();
			st.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public static void addToBDUser(String l,String n,String p,String psw){
		
		try {
			Connection con = Database.getMySQLConnection();
			String query = "insert into user (login,nom,prenom,psw) values (?, ?, ?, PASSWORD(?))";
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, l);
			st.setString(2, n);
			st.setString(3, p);
			st.setString(4, psw);
			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean checkPassword(String login, String psw){
		boolean rep = false;
		try {
			Connection con = Database.getMySQLConnection();
			String query = "select * from user where login= ? and psw = PASSWORD(?)";
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, login);
			st.setString(2, psw);
			
			ResultSet rs = st.executeQuery();
			
			if(rs.next()) {
				rep =  true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rep;
	}
	
	public static int isRoot(String user){
		return 0;
	}
	
	public static String insererConnexion(String login,int root){
		String key = "";
		try {
			if(!isOnline(getIdUserL(login))){
				key = UUID.randomUUID().toString();
				Connection con = Database.getMySQLConnection();
				String query = "insert into online (id_user, user_key, is_root, date_connect) values (?, ?, ? , NOW())";
				PreparedStatement st = con.prepareStatement(query);
				st.setInt(1, getIdUserL(login));
				st.setString(2, key);
				st.setInt(3, isRoot(login));
				st.executeUpdate();
				st.close();
				con.close();
			}else{
				return User.getKey();
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return key;
	}
	
	public static void logout(String key) {
		try {
			Connection con = Database.getMySQLConnection();
			String query = "delete from online where user_key= ?";
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, key);
			st.executeUpdate();
			st.close();
			con.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	public static JSONArray getProfil(String login){
		JSONArray tab = new JSONArray();
		tab.add(FriendTools.listFriend(login));
		tab.add(MessageTools.listMessage(login));
		return tab;
	}
	
	public static boolean isOnline(int id_user){
		if(userExists(id_user)){
			try
			{
				Connection con = Database.getMySQLConnection();
				String query = "select * from online where id_user = ? and date_connect >= DATE_ADD(NOW(), INTERVAL -3600 SECOND)";
				PreparedStatement st = con.prepareStatement(query);
				st.setInt(1, id_user);
				ResultSet r = st.executeQuery();
				if(r.next()){
					return true;
				}
				st.close();
				con.close();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
		}
		logout(User.getKey());
		return false;
	}
	
	public static int getIdUserK(String key){
		if(key != null){
			try
			{
				Connection con = Database.getMySQLConnection();
				String query = "Select id_user from online where user_key = ?";
				PreparedStatement st = con.prepareStatement(query);
				st.setString(1, key);
				ResultSet r = st.executeQuery();
				if(r.next()){
					return r.getInt("id_user");
				}
				st.close();
				con.close();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	
	public static int getIdUserL(String login){
		if(login != null){
			try
			{
				Connection con = Database.getMySQLConnection();
				String query = "Select id_user from user where login = ?";
				PreparedStatement st = con.prepareStatement(query);
				st.setString(1, login);
				ResultSet r = st.executeQuery();
				if(r.next()){
					return r.getInt("id_user");
				}
				st.close();
				con.close();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
}
