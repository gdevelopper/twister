package servlets.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import services.User;
import servicesTools.ErrorTools;

public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map <String, String[]> args = request.getParameterMap();
//		response.setContentType("text/json"); // Ce quoi être fait
		response.setContentType("text/html");//Pour le test 
		PrintWriter out = response.getWriter();
		
		
		if((args.containsKey("login") && args.containsKey("nom") && args.containsKey("prenom") && args.containsKey("psw")) && !(request.getParameter("login").isEmpty() || request.getParameter("nom").isEmpty() || request.getParameter("prenom").isEmpty() || request.getParameter("psw").isEmpty())){
			String l = request.getParameter("login");
			String n = request.getParameter("nom");
			String p = request.getParameter("prenom");
			String psw = request.getParameter("psw");
			out.print(User.createUser(l, n, p, psw).toString());
		}else{
			out.print(ErrorTools.serviceRefused("Veuillez bien remplir tous les champs", -1).toString());
			out.println("<h1>Ajout d'utilisateur</h1>");
			out.println("<form method = 'GET'>");
			out.println("<input type='text' name='login' placeholder='Tapez votre Login'/>");
			out.println("<input type='text' name='nom' placeholder='Tapez votre Nom'/>");
			out.println("<input type='text' name='prenom' placeholder='Tapez votre Prenom'/>");
			out.println("<input type='password' name='psw' placeholder='Tapez votre Mot de passe'/>");
			out.println("<input type='submit'/>");
			out.println("</form>");
		}		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
