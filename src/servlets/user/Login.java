package servlets.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.User;
import servicesTools.ErrorTools;

/**
 * Servlet implementation class Login
 */

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.setContentType("text/json");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		
		
		if(request.getParameter("login") != null && request.getParameter("mdp") != null && !(request.getParameter("login").isEmpty() || request.getParameter("mdp").isEmpty())){
			String user = request.getParameter("login");
			String mdp = request.getParameter("mdp");
			out.print(User.login(user, mdp).toString());
		}else{
			out.print(ErrorTools.serviceRefused("Le mot de passe ou le login est incorrect", -1).toString());
			out.println("<h1>Se Connecter</h1>");
			out.println("<form method = 'GET'>");
			out.println("<input type='text' name='login' placeholder='Tapez votre Login'/>");
			out.println("<input type='password' name='mdp' placeholder='Tapez votre Password'/>");
			out.println("<input type='submit'/>");
			out.println("</form>");
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
