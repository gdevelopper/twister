package servlets.friend;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import services.Friend;
import servicesTools.ErrorTools;

public class AddFriend extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/json");
		PrintWriter out = response.getWriter();
		if(request.getParameter("key") != null && request.getParameter("login_friend") != null){
			String key = request.getParameter("key");
			String loginFriend = request.getParameter("login_friend");
			out.print(Friend.addFriend(key, loginFriend).toString());
		}else{
			out.print(ErrorTools.serviceRefused("Veuillez bien remplir tous les champs", -1).toString());
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
