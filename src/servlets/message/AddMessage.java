package servlets.message;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import services.Message;
import servicesTools.ErrorTools;

public class AddMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/json");
		PrintWriter out = response.getWriter();
		if(request.getParameter("key") != null && request.getParameter("message") != null){
			String key = request.getParameter("key");
			String message = request.getParameter("message");
			out.print(Message.addMessage(key, message).toString());
		}else{
			out.print(ErrorTools.serviceRefused("Veuillez bien remplir tous les champs", -1).toString());
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
