package servlets.message;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import services.Message;
import servicesTools.ErrorTools;

public class AddComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/json");
		PrintWriter out = response.getWriter();
		if(request.getParameter("login_submiter") != null && request.getParameter("id_message") != null && request.getParameter("comment") != null){
			String loginSubmiter = request.getParameter("login_submiter");
			String idMessage = request.getParameter("id_message");
			String comment = request.getParameter("comment");
			out.print(Message.addComment(loginSubmiter, idMessage, comment).toString());
		}else{
			out.print(ErrorTools.serviceRefused("Veuillez bien remplir tous les champs", -1).toString());
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
