package servicesTools;
import org.json.simple.JSONObject;


public class ErrorTools {
	@SuppressWarnings("unchecked")
	public static JSONObject serviceRefused(String msg, int idError){
		JSONObject reponse = new JSONObject();
		try{
			reponse.put("status", "KO");
			reponse.put("message", msg);
			reponse.put("idError ", idError);
		}catch(Exception e){
			System.out.println(e);
		}
		return reponse;
	}
}
