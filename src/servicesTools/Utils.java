package servicesTools;

import java.util.Date;
import java.util.GregorianCalendar;

public final class Utils {
	public static String getTime(){
		GregorianCalendar c = new GregorianCalendar();
		Date d = c.getTime();
		return d.toString();
	}
}
